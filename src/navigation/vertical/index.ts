import HomeOutline from 'mdi-material-ui/HomeOutline'
import Account from 'mdi-material-ui/Account'
import Store from 'mdi-material-ui/Store'
import BackspaceReverse from 'mdi-material-ui/BackspaceReverse'

import { VerticalNavItemsType } from 'src/@core/layouts/types'

const navigation = (): VerticalNavItemsType => {
  return [
    {
      title: 'Dashboard',
      icon: HomeOutline,
      path: '/'
    },
    {
      title: 'User',
      icon: Account,
      path: '/users'
    },
    {
      title: 'Shop',
      icon: Store,
      path: '/shops'
    },
    {
      title: 'Banner',
      icon: BackspaceReverse,
      path: '/banners'
    }
  ]
}

export default navigation
