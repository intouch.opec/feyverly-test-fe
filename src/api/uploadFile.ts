import callApi from "src/utils/callApi";

export const uploadFile = async (file: File[]) => {
  const formData = new FormData();
  formData.append("file", file[0]);
  return callApi.post("/upload-file/upload", formData, {
    headers: { "Content-Type": "multipart/form-data" },
  });
};
