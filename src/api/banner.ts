import callApi from "src/utils/callApi";

export const getBanners = async (page: number, take: number) => {
  return callApi.get("/banners", { params: { page, take } });
};

export const getBanner = async (id: string) => {
  return callApi.get(`/banners/${id}`);
};

export const createBanner = async (createBanner: any) => {
  return callApi.post(`/banners`, createBanner);
};

export const updateBanner = async (id: string, createBanner: any) => {
  return callApi.put(`/banners/${id}`, createBanner);
};

export const removeBanner = async (id: string) => {
  return callApi.delete(`/banners/${id}`);
};

export const removeBannerImage = async (id: string) => {
  return callApi.delete(`/banners/${id}/url`);
};