import { LoginDto } from 'src/dto/login.gto';
import axios from "axios";

export const login = async (body: LoginDto) => {
  return axios.post(`http://localhost:3000/auth/login`, body)
}