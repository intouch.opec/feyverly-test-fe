import { CreateUserDto } from "src/dto/createUser.dto";
import callApi from "src/utils/callApi";

export const getUsers = async (page: number, take: number) => {
  return callApi.get("/users", { params: { page, take } });
};

export const getUser = async (id: string) => {
  return callApi.get(`/users/${id}`);
};

export const createUser = async (createUser: any) => {
  createUser.role = "admin";
  return callApi.post(`/users/signup`, createUser);
};

export const updateUser = async (id: string, createUser: any) => {
  createUser.role = "admin";
  return callApi.put(`/users/${id}`, createUser);
};

export const removeUser = async (id: string) => {
  return callApi.delete(`/users/${id}`);
};
