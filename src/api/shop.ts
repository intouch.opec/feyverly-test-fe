import callApi from "src/utils/callApi";

export const getShops = async (page: number, take: number) => {
  return callApi.get("/shops", { params: { page, take } });
};

export const getShop = async (id: string) => {
  return callApi.get(`/shops/${id}`);
};

export const createShop = async (createShop: any) => {
  return callApi.post(`/shops`, createShop);
};

export const updateShop = async (id: string, createShop: any) => {
  return callApi.put(`/shops/${id}`, createShop);
};

export const removeShop = async (id: string) => {
  return callApi.delete(`/shops/${id}`);
};
