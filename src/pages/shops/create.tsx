import Grid from '@mui/material/Grid'
import DatePickerWrapper from 'src/@core/styles/libs/react-datepicker'
import ShopForm from 'src/Form/ShopForm'
import 'react-datepicker/dist/react-datepicker.css'

export default () => {
  return <DatePickerWrapper>
    <Grid container justifyContent='center' spacing={6}>
      <Grid item xs={12} md={6}>
        <ShopForm />
      </Grid>
    </Grid>
  </DatePickerWrapper>
}