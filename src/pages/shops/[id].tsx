import Grid from '@mui/material/Grid'
import DatePickerWrapper from 'src/@core/styles/libs/react-datepicker'
import ShopForm from 'src/Form/ShopForm'
import 'react-datepicker/dist/react-datepicker.css'
import { useRouter } from 'next/router'
import { useQuery } from '@tanstack/react-query'
import { getShop } from 'src/api/shop'

export default () => {
  const { query } = useRouter()
  const shop = useQuery(['shop', query.id], async () => await getShop(query.id as string).then(res => res.data))

  return <DatePickerWrapper>
    <Grid container justifyContent='center' spacing={6}>
      <Grid item xs={12} md={6}>
        {shop.data && <ShopForm shop={shop.data} />}
      </Grid>
    </Grid>
  </DatePickerWrapper>
}