import Grid from '@mui/material/Grid'
import DatePickerWrapper from 'src/@core/styles/libs/react-datepicker'
import UserForm from 'src/Form/UserForm'
import 'react-datepicker/dist/react-datepicker.css'

export default () => {
  return <DatePickerWrapper>
    <Grid container justifyContent='center' spacing={6}>
      <Grid item xs={12} md={6}>
        <UserForm />
      </Grid>
    </Grid>
  </DatePickerWrapper>
}