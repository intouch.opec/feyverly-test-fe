import Grid from '@mui/material/Grid'
import Card from '@mui/material/Card'
import CardHeader from '@mui/material/CardHeader'
import Button from '@mui/material/Button'
import { useRouter } from 'next/router'
import Paper from '@mui/material/Paper'
import Table from '@mui/material/Table'
import TableRow from '@mui/material/TableRow'
import TableHead from '@mui/material/TableHead'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import { useQuery } from "@tanstack/react-query";
import { getUsers, removeUser } from 'src/api/user'
import map from 'lodash/map'
import * as dayjs from 'dayjs'
import Pen from 'mdi-material-ui/Pen'
import Close from 'mdi-material-ui/Close'
import { ChangeEvent, useState } from 'react'
import TablePagination from '@mui/material/TablePagination'
import Skeleton from '@mui/material/Skeleton';

export default () => {
  const router = useRouter()
  const [page, setPage] = useState<number>(0)
  const [rowsPerPage, setRowsPerPage] = useState<number>(10)

  const { refetch, data: result, isFetching } = useQuery({
    queryKey: ['user', page, rowsPerPage],
    queryFn: () =>
      getUsers(page + 1, rowsPerPage).then(res => res.data)
  })

  const handleChangePage = (_: unknown, newPage: number) => {
    setPage(newPage)
  }

  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(+event.target.value)
    setPage(1)
  }

  function removeUserHandle(id: string) {
    removeUser(id).then(() => refetch())
  }

  const renderRecord = (users: any[]) => map(users, row => (
    <TableRow
      key={row.id}
      sx={{
        '&:last-of-type td, &:last-of-type th': {
          border: 0
        }
      }}
    >
      <TableCell component='th' scope='row'>
        {row.username}
      </TableCell>
      <TableCell>{row.email}</TableCell>
      <TableCell align='right'>{dayjs(row.createdAt).format('DD-MM-YYYY')}</TableCell>
      <TableCell align='right'>{dayjs(row.updatedAt).format('DD-MM-YYYY')}</TableCell>
      <TableCell align='right'>
        <Pen onClick={() => router.push(`/users/${row.id}`)} />
        <Close onClick={() => removeUserHandle(row.id)} />
      </TableCell>
    </TableRow>
  ))

  const renderLoding = (columnCount: number) => {
    let record = []
    for (let index = 0; index < columnCount; index++) {
      record.push(<TableCell key={`loding-${index}`}>
        <Skeleton />
      </TableCell>)
    }
    return record
  }

  return <Grid container spacing={6}>
    <Grid container
      direction="row"
      justifyContent="flex-end"
      alignItems="center">
      <Grid item xs={2}>
        <Button
          fullWidth
          size='large'
          variant='contained'
          onClick={() => router.push('/users/create')}
        >
          Create User
        </Button>
      </Grid>
    </Grid>
    <Grid item xs={12}>
      <Card>
        <CardHeader title='Users' titleTypographyProps={{ variant: 'h6' }} />
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label='simple table'>
            <TableHead>
              <TableRow>
                <TableCell>username</TableCell>
                <TableCell>email</TableCell>
                <TableCell align='right'>createdAt</TableCell>
                <TableCell align='right'>updatedAt</TableCell>
                <TableCell >action</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {isFetching ? <TableRow>
                {renderLoding(5)}
              </TableRow> :
                renderRecord(result?.data)
              }
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 50]}
          component='div'
          count={result?.meta?.itemCount ? parseInt(result?.meta?.itemCount) : 1}
          rowsPerPage={rowsPerPage}
          page={result?.meta?.page ? parseInt(result?.meta?.page) - 1 : 0}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Card>
    </Grid>
  </Grid>
}
