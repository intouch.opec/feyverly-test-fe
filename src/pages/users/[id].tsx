import Grid from '@mui/material/Grid'
import DatePickerWrapper from 'src/@core/styles/libs/react-datepicker'
import UserForm from 'src/Form/UserForm'
import 'react-datepicker/dist/react-datepicker.css'
import { useRouter } from 'next/router'
import { useQuery } from '@tanstack/react-query'
import { getUser } from 'src/api/user'

export default () => {
  const { query } = useRouter()
  const user = useQuery(['user',query.id], async () => await getUser(query.id as string).then(res => res.data))

  return <DatePickerWrapper>
    <Grid container justifyContent='center' spacing={6}>
      <Grid item xs={12} md={6}>
        {user.data && <UserForm user={user.data} />}
      </Grid>
    </Grid>
  </DatePickerWrapper>
}