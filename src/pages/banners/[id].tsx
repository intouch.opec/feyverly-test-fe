import Grid from '@mui/material/Grid'
import DatePickerWrapper from 'src/@core/styles/libs/react-datepicker'
import BannerForm from 'src/Form/BannerForm'
import 'react-datepicker/dist/react-datepicker.css'
import { useRouter } from 'next/router'
import { useQuery } from '@tanstack/react-query'
import { getBanner } from 'src/api/banner'

export default () => {
  const { query } = useRouter()
  const banner = useQuery(['banner', query.id],
    async () => await getBanner(query.id as string).then(res => res.data),
  {enabled: !!query.id})
  
  return <DatePickerWrapper>
    <Grid container justifyContent='center' spacing={6}>
      <Grid item xs={12} md={6}>
        {banner.isLoading ? 'loading....' :
        banner.data && <BannerForm banner={banner.data} refetch={banner.refetch} /> }
      </Grid>
    </Grid>
  </DatePickerWrapper>
}