import axios from "axios";
import Cookies from 'js-cookie';

const instance = axios.create({
  baseURL: 'http://localhost:3000',
});

instance.interceptors.request.use(async (config) => {
  const token = `${await Cookies.get('token')}`;
  if (token) {
    // eslint-disable-next-line no-param-reassign
    config.headers['Authorization'] = `Bearer ${token}`;
  }

  return config;
});

instance.interceptors.response.use(
  (response) => {
    return response;
  },
  async (error) => {
    const originalRequest = error.config;
    if (error?.response?.status === 401 && !originalRequest.retry) {      
      window.location.href = "/login";
    }
    return Promise.reject(error);
  }
);

export default instance;

