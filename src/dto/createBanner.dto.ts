export type CreateBannerDto = {
  name: string;
  shopId: string;
  url: string;
}
