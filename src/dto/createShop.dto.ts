export type CreateShopDto = {
  name: string;
  lat: string;
  lng: string;
}
