import Box from '@mui/material/Box'
import Card from '@mui/material/Card'
import Grid from '@mui/material/Grid'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import CardHeader from '@mui/material/CardHeader'
import CardContent from '@mui/material/CardContent'
import { useForm, Controller } from "react-hook-form"
import * as yup from "yup"
import { yupResolver } from "@hookform/resolvers/yup"
import { useRouter } from 'next/router'
import { createBanner, updateBanner, removeBannerImage } from 'src/api/banner'
import { uploadFile } from 'src/api/uploadFile'
import Close from 'mdi-material-ui/Close'
import { useQueryClient } from '@tanstack/react-query'

type FormValues = {
  name: string;
  file?: File[];
  url?: string;
  id?: string;
}

const schema = yup.object().shape({
  name: yup.string().required()
});

type Props = {
  banner?: FormValues
  refetch?: any
}

const BannerForm = ({ banner }: Props) => {
  const { push, query } = useRouter()
  const queryClient = useQueryClient();

  const { handleSubmit, control, formState: { errors } } = useForm<FormValues>(
    {
      resolver: yupResolver(schema),
      defaultValues: banner,
    }
  )

  const submit = handleSubmit(async (form: FormValues) => {
    if (form.file) {
      const res = await uploadFile(form.file)
      form.url = res.data.data
    }
    if (!query.id) {
      await createBanner(form)
    } else {
      await updateBanner(query.id as string, form)
    }
    push('/banners')
  })

  const handleRemoveImage = () => removeBannerImage(banner!.id || '')
    .then(() => queryClient.refetchQueries(['banner', query.id])
)

  return (
    <Card>
      <CardHeader title='Banner Form' titleTypographyProps={{ variant: 'h6' }} />
      <CardContent>
        <form onSubmit={submit}>
          <Grid container spacing={5}>
            <Grid item xs={12}>
              <Controller
                control={control}
                name='name'
                render={({ field }) => (
                  <TextField
                    fullWidth
                    label='Name'
                    placeholder='BannerName1234'
                    error={!!errors.name?.message}
                    helperText={errors.name?.message}
                    {...field}
                  />
                )} />
            </Grid>
            <Grid item xs={12}>
              {banner?.url ? <div style={{ display: 'inline-block', position: 'relative' }}>
                <img style={{ width: '100%', height: '100%' }} src={`http://localhost:3000/${banner!.url}`} />
                <Close
                  onClick={handleRemoveImage}
                  style={{
                  position: 'absolute', top: '0',
                  right: '0'
                }}  />
              </div>
                :
                <Controller
                  control={control}
                  name='file'
                  render={({ field }) => (
                    <input type="file" onChange={(e) => field.onChange(e?.target.files)} />
                  )}
                />}
            </Grid>
            <Grid item xs={12}>
              <Box
                sx={{
                  gap: 5,
                  display: 'flex',
                  flexWrap: 'wrap',
                  alignItems: 'center',
                  justifyContent: 'space-between'
                }}
              >
                <Button type='submit' variant='contained' size='large'>
                  Create Banner
                </Button>
              </Box>
            </Grid>
          </Grid>
        </form>
      </CardContent>
    </Card>
  )
}

export default BannerForm
