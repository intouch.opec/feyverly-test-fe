import { useState, Fragment, useEffect } from 'react'
import Box from '@mui/material/Box'
import Card from '@mui/material/Card'
import CardHeader from '@mui/material/CardHeader'
import CardContent from '@mui/material/CardContent'
import Grid from '@mui/material/Grid'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import { useForm, Controller } from "react-hook-form"
import * as yup from "yup"
import { yupResolver } from "@hookform/resolvers/yup"
import { useRouter } from 'next/router'
import { createShop, updateShop } from 'src/api/shop'
import {
  useLoadScript,
  GoogleMap,
  Marker,
} from "@react-google-maps/api";

type FormValues = {
  name: string;
  lng: string;
  lat: string;
}

const schema = yup.object().shape({
  name: yup.string()
    .required(),
  lng: yup.string()
    .required(),
  lat: yup.string()
    .required(),
});

type Props = { shop?: FormValues }
const ShopForm = ({ shop }: Props) => {
  const { push, query } = useRouter()
  const [_, setMapRef] = useState(null);
  const [center] = useState(shop ? {lat: parseFloat(shop.lat), lng: parseFloat(shop.lng)} : { "lat": 13.753319829799162, "lng": 100.50056238004609 });
  const [zoom] = useState(5);
  const [clickedLatLng, setClickedLatLng] = useState(shop ? {lat: parseFloat(shop.lat), lng: parseFloat(shop.lng)} : null);
  const { isLoaded } = useLoadScript({
    googleMapsApiKey: ""
  });

  const loadHandler = (map: any) => {
    setMapRef(map);
  };

  const { handleSubmit, control, setValue, formState: { errors } } = useForm<FormValues>(
    {
      resolver: yupResolver(schema),
      defaultValues: shop,
    }
  )

  useEffect(() => {
    if (clickedLatLng) {
      setValue('lat', `${clickedLatLng?.lat}`)
      setValue('lng', `${clickedLatLng?.lng}`)
    }
  }, [clickedLatLng])

  const submit = handleSubmit((form: FormValues) => {
    if (!query.id) {
      createShop(form)
    } else {
      updateShop(query.id as string, form)
    }
    push('/shops')
  })

  const renderMap = () => {
    return (
      <GoogleMap
        onLoad={loadHandler}
        onClick={e => setClickedLatLng(e?.latLng?.toJSON())}
        center={center}
        zoom={zoom}
        mapContainerStyle={{
          height: "40vh",
          width: "100%"
        }}
      >
        {clickedLatLng && <Marker
          position={clickedLatLng}
        />}
      </GoogleMap>
    );
  };

  return (
    <Card>
      <CardHeader title='Shop Form' titleTypographyProps={{ variant: 'h6' }} />
      <CardContent>
        <form onSubmit={submit}>
          <Grid container spacing={5}>
            <Grid item xs={12}>
              <Controller
                control={control}
                name='name'
                render={({ field }) => (
                  <TextField
                    fullWidth
                    label='Name'
                    placeholder='ShopName1234'
                    error={!!errors.name?.message}
                    helperText={errors.name?.message}
                    {...field}
                  />
                )} />
            </Grid>
            <Grid item xs={12}>
              {isLoaded ? renderMap() : null}
            </Grid>
            <Grid item xs={12}>
              <Box
                sx={{
                  gap: 5,
                  display: 'flex',
                  flexWrap: 'wrap',
                  alignItems: 'center',
                  justifyContent: 'space-between'
                }}
              >
                <Button type='submit' variant='contained' size='large'>
                  Create Shop
                </Button>
              </Box>
            </Grid>
          </Grid>
        </form>
      </CardContent>
    </Card>
  )
}

export default ShopForm
