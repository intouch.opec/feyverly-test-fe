import { useState } from 'react'
import Box from '@mui/material/Box'
import Card from '@mui/material/Card'
import Grid from '@mui/material/Grid'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import CardHeader from '@mui/material/CardHeader'
import InputLabel from '@mui/material/InputLabel'
import IconButton from '@mui/material/IconButton'
import CardContent from '@mui/material/CardContent'
import FormControl from '@mui/material/FormControl'
import OutlinedInput from '@mui/material/OutlinedInput'
import InputAdornment from '@mui/material/InputAdornment'
import FormHelperText from '@mui/material/FormHelperText'
import EyeOutline from 'mdi-material-ui/EyeOutline'
import EyeOffOutline from 'mdi-material-ui/EyeOffOutline'
import { useForm, Controller } from "react-hook-form"
import * as yup from "yup"
import validator from 'validator'
import { yupResolver } from "@hookform/resolvers/yup"
import { createUser, updateUser } from 'src/api/user'
import { useRouter } from 'next/router'

type FormValues = {
  username: string;
  email: string;
  password: string;
  role?: string;
  confirmPass: string;
}

const schema = yup.object().shape({
  role: yup.string(),
  username: yup.string()
    .required(),
  email: yup.string()
    .email()
    .required()
    .test((value: string) => validator.isEmail(value)),
  password: yup.string()
    .required("Password is required")
    .min(4, "Password length should be at least 4 characters")
    .max(12, "Password cannot exceed more than 12 characters"),
  confirmPass: yup.string()
    .required("Confirm Password is required")
    .min(4, "Password length should be at least 4 characters")
    .max(12, "Password cannot exceed more than 12 characters")
    .oneOf([yup.ref("password")], "Passwords do not match")
});

type Props = {
  user?: FormValues;
}

const UserForm = (props: Props) => {
  const [showPassword, setShowPassword] = useState<boolean>(false)
  const { push, query } = useRouter()
  const [showConfirmPass, setShowConfirmPass] = useState<boolean>(false)
  const { handleSubmit, control, formState: { errors } } = useForm<FormValues>(
    {
      resolver: yupResolver(schema),
      defaultValues: props.user,
    }
  )
  const submit = handleSubmit((form: FormValues) => {
    if (!query.id) {
      createUser(form)
    } else {
      updateUser(query.id as string, form)
    }
    push('/users')
  })

  return (
    <Card>
      <CardHeader title='User Form' titleTypographyProps={{ variant: 'h6' }} />
      <CardContent>
        <form onSubmit={submit}>
          <Grid container spacing={5}>
            <Grid item xs={12}>
              <Controller
                control={control}
                name='username'
                render={({ field }) => (
                  <TextField
                    fullWidth
                    label='Name'
                    placeholder='UserName1234'
                    error={!!errors.username?.message}
                    helperText={errors.username?.message}
                    {...field}
                  />
                )} />
            </Grid>
            <Grid item xs={12}>
              <Controller
                control={control}
                name='email'
                render={({ field }) => (
                  <TextField
                    fullWidth
                    type='email'
                    label='Email'
                    placeholder='carterleonard@gmail.com'
                    error={!!errors.email?.message}
                    helperText={errors.email?.message || 'You can use letters, numbers & periods'}
                    {...field}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12}>
              <Controller
                control={control}
                name='password'
                render={({ field: { onChange, value } }) => (
                  <FormControl fullWidth>
                    <InputLabel error={!!errors.password?.message} htmlFor='form-layouts-basic-password'>Password</InputLabel>
                    <OutlinedInput
                      error={!!errors.password?.message}
                      label='Password'
                      value={value}
                      id='form-layouts-basic-password'
                      onChange={onChange}
                      type={showPassword ? 'text' : 'password'}
                      aria-describedby='form-layouts-basic-password-helper'
                      endAdornment={
                        <InputAdornment position='end'>
                          <IconButton
                            edge='end'
                            onClick={() => setShowPassword(state => !state)}
                            aria-label='toggle password visibility'
                          >
                            {showPassword ? <EyeOutline /> : <EyeOffOutline />}
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                    <FormHelperText error={!!errors.password?.message} id='form-layouts-basic-password-helper'>
                      {errors.password?.message || `Use 8 or more characters with a mix of letters, numbers & symbols`}
                    </FormHelperText>
                  </FormControl>
                )}
              />
            </Grid>
            <Grid item xs={12}>
              <Controller
                control={control}
                name='confirmPass'
                render={({ field: { onChange, value } }) => (
                  <FormControl fullWidth>
                    <InputLabel error={!!errors.confirmPass?.message} htmlFor='form-layouts-confirm-password'>Confirm Password</InputLabel>
                    <OutlinedInput
                      error={!!errors.confirmPass?.message}
                      label='Confirm Password'
                      value={value}
                      id='form-layouts-confirm-password'
                      onChange={onChange}
                      aria-describedby='form-layouts-confirm-password-helper'
                      type={showConfirmPass ? 'text' : 'password'}
                      endAdornment={
                        <InputAdornment position='end'>
                          <IconButton
                            edge='end'
                            onClick={() => setShowConfirmPass(state => !state)}
                            aria-label='toggle password visibility'
                          >
                            {showConfirmPass ? <EyeOutline /> : <EyeOffOutline />}
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                    <FormHelperText error={!!errors.confirmPass?.message} id='form-layouts-confirm-password-helper'>
                      {errors.confirmPass?.message || `Make sure to type the same password as above`}
                    </FormHelperText>
                  </FormControl>
                )}
              />
            </Grid>
            <Grid item xs={12}>
              <Box
                sx={{
                  gap: 5,
                  display: 'flex',
                  flexWrap: 'wrap',
                  alignItems: 'center',
                  justifyContent: 'space-between'
                }}
              >
                <Button type='submit' variant='contained' size='large'>
                  Create User
                </Button>
              </Box>
            </Grid>
          </Grid>
        </form>
      </CardContent>
    </Card>
  )
}

export default UserForm
